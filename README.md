# Gateway application
This project is a gateway for the Rent Car application
It uses the Netflix Zuul as API Gateway. 

## Requirements
* java 11
* Maven

## Running the application
1. Clone the repo
2. Run the command:
> mvn spring-boot:run

